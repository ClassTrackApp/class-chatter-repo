//
//  Teacher+CoreDataProperties.m
//  ClassChatter App
//
//  Created by Adam Goldberg on 2015-11-11.
//  Copyright © 2015 Adam Goldberg. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Teacher+CoreDataProperties.h"

@implementation Teacher (CoreDataProperties)

@dynamic emailTemplateBad;
@dynamic emailTemplateGood;
@dynamic limitForBadEmails;
@dynamic limitforGoodEmails;
@dynamic principalEmail;
@dynamic schoolClasses;
@dynamic students;

@end
